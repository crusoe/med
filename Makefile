#!/usr/bin/make -f

# export GENCONTROL_OPTS = --udd

testopts:
	echo ${GENCONTROLOPTS}
	if [ "$$GENCONTROLOPTS" != "" ] ; then \
	    echo "GENCONTROLOPTS = ${GENCONTROLOPTS} ( $(GENCONTROLOPTS) )" ; \
	fi
	echo "Now test the underscore variable"
	echo ${GENCONTROL_OPTS}
	if [ "$$GENCONTROL_OPTS" != "" ] ; then \
	    echo "GENCONTROL_OPTS = ${GENCONTROL_OPTS} ( $(GENCONTROL_OPTS) )" ; \
	fi

BLENDMAKEFILE=/usr/share/blends-dev/Makefile

CheckBLENDMakefile := $(shell if [ -e $(BLENDMAKEFILE) ] ; then echo 1 ; else echo 0 ; fi)
ifeq ($(CheckBLENDMakefile),1)
    include $(BLENDMAKEFILE)
else
    err := $(shell echo "$(BLENDMAKEFILE) is missing.  Please install blends-dev package!")
endif

dummy:
	@echo $(err)

dep_data:
	make statusdump
